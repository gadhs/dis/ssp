= 3.1. Access Control
:imagesdir: ../../images
:!paragraph:
:doctitle: Access Control
:docfilesuffix: .adoc
:section: 3.1.

////
Instructions

For each control replace the default text line (include::../../partials/default_control.adoc[]) with the appropriate language either describing the control implementation, or describing why the control does not apply.

In order to import the correct table, set the tag value to none, implemented, planned, or na based on which box you want to be checked.
////

== {section}{counter:paragraph}. Limit system access to authorized users, processes acting on behalf of authorized users, and devices (including other systems). 

include::../../partials/tables.adoc[tag=implemented]
DIS system access is limited to authorized agency staff and contractors via centrally-managed Active Directory credentials.
DIS KIOSK constituent accounts are authorized and managed via the Georgia Gateway system (https://gateway.ga.gov).
Actions for processes acting on behalf of users, such as document uploads, are limited to use cases outlined in the _DIS Use Cases_ document.
DIS KIOSK hardware devices must be connected to the State of Georgia (SOG) network in DHS field offices to have access to the DIS system.

AWS Managed Services staff are authorized to access the DIS cloud infrastructure components to provide management and support services as outlined in the service contract.

== {section}{counter:paragraph}. Limit system access to the types of transactions and functions that authorized users are permitted to execute.

include::../../partials/tables.adoc[tag=implemented]
DIS application access is limited to use cases outlined in the _DIS Use Cases_ document.

Service Control Policies (SCPs) are used for additional permission management for the AWS DIS system environment. 
SCPs are used to define additional guardrails to restrict and the limit the actions that authenticated users and administrators can perform.
The specific SCP restrictions in place for the DIS system environment are detailed in the _DHS DIS Infrastructure Design_ document.

== {section}{counter:paragraph}. Control the flow of CUI in accordance with approved authorizations.

include::../../partials/tables.adoc[tag=implemented]
The flow of CUI data is outlined in the _DIS Use Cases_ document, which describes the data acquisition and processing workflow for each use case.
Firewall rules (AWS Security Groups) are used to control the network traffic as detailed in the _DHS DIS Infrastructure Design_ document.

== {section}{counter:paragraph}. Separate the duties of individuals to reduce the risk of malevolent activity without collusion.

include::../../partials/tables.adoc[tag=implemented]
For DHS staff, their job role determines the Active Directory group membership to assign the appropriate level of access.
Active Directory group member is controlled by Managers within that workgroup to allow/remove group membership.
Within the DIS application, CUI document access is separated at the agency level (DFCS, DCH, DPH, Child Welfare, DECAL) so that users only have access to the appropriate documents for their agency and access level.
Additional restrictions based on user role with agency are incorporated into DIS application to restrict CUI document access for each agency's users.

Additionally, for access to DIS servers (i.e. EC2 instances), DIS staff must open a change request ticket with AWS Managed Services (AMS) to request either admin (read/write) or read-only temporary access permissions to the EC2 resource.


== {section}{counter:paragraph}. Employ the principle of least privilege, including for specific security functions and privileged accounts.

include::../../partials/tables.adoc[tag=implemented]
As detailed in _Section 1.3.1_ of this SSP document, DHS user roles are defined for least privilege access to the DIS application and AWS DIS resources.  
Permission to the roles are granted by manager approval based on job function of the individual.

== {section}{counter:paragraph}. Use non-privileged accounts or roles when accessing non-security functions.

include::../../partials/tables.adoc[tag=implemented]
For access to DIS servers (i.e. EC2 instances), DIS staff must open a change request ticket with AWS Managed Services (AMS) to request either admin (read/write) or read-only temporary access permissions to the EC2 resource.
The requested permission level is based on the type of change that will performed on the EC2 resource.
The access that is granted includes a time restriction (8 hour default) after which access must be re-requested.

== {section}{counter:paragraph}. Prevent non-privileged users from executing privileged functions and audit the execution of such functions.

include::../../partials/tables.adoc[tag=implemented]
Privileged functions require elevated permissions to execute.  
Within the DIS application, user actions are restricted based on the level of access for their role (read/write versus read-only).
DIS database contains an audit log table to record user activity.

In the AWS environment, role permissions and Service Control Policies (SCP) determine what actions a user may perform. Additionally, all user actions on AWS resources are performed via Application Control Interface (API) calls and are logged to CloudTrail.

== {section}{counter:paragraph}. Limit unsuccessful logon attempts.

include::../../partials/tables.adoc[tag=implemented]
The DIS application limits unsuccessful login attempts to 3 tries.
DIS Admin team can only access the DIS application via the State VPN which requires AAL-2 compliant MFA.

This control provides a means to prevent brute force attacks against accounts that are authenticated via a user ID and password. 
AWS Managed Services operator access requires multi-factor authentication, using a hardware token, as a mitigation for the need to implement user account lockout.

== {section}{counter:paragraph}. Provide privacy and security notices consistent with applicable CUI rules.

include::../../partials/tables.adoc[tag=implemented]
The following security statement is displayed to users on DIS KIOSK hardware and as a footer on the DIS application webpages:
----
Information provided on georgia.gov portal websites is intended to allow the public convenient access to state government services and information. While all attempts are made to provide accurate, current and reliable information we cannot guarantee that the information will be error-free. Therefore, the State of Georgia, the Georgia Technology Authority, and their respective employees, officers and agencies expressly deny any warranty of the accuracy, reliability or timeliness of any information published through their websites and shall not be held liable for any losses caused by reliance upon the accuracy, reliability or timeliness of such information.
In accordance with Federal law and U. S. Department of Agriculture (USDA) and U.S. Department of Health and Human Services (HHS) policy, this institution is prohibited from discriminating on the basis of race, color, national origin, sex, age, or disability. Under the Food Stamp Act and USDA policy, discrimination is prohibited also on the basis of religion or political beliefs.
To file a complaint of discrimination, contact USDA or HHS. Write USDA, Office of the Assistant Secretary of Civil Rights, 1400 Independence Avenue, S.W., Washington D.C. 20250-9410 or call (866) 632-9992 (voice), (800) 877-8339 (TTY) or (202) 720-2600 (voice and TDD). Write US Department of Health and Human Services, Office for Civil Rights, Centralized Case Management Operations, Suite 515F, HHH Building, 200 Independence Avenue, S.W., Washington, D.C. 20201 or call (800) 368-1019 (voice) or (800) 537-7697 (TDD). USDA and HHS are equal opportunity providers and employers.
----

== {section}{counter:paragraph}. Use session lock with pattern-hiding displays to prevent access and viewing of data after period of inactivity. 

include::../../partials/tables.adoc[tag=implemented]
DHS staff laptops and AMS staff workstations automatically enable screen lock after specified user inactivity period.
DIS application timeout period is 10 minutes.

== {section}{counter:paragraph}. Terminate (automatically) a user session after a defined condition. 

include::../../partials/tables.adoc[tag=implemented]
DIS application user sessions timeout after 10 minutes of inactivity.
Remote user VPN connections have a maximum duration of 24 hours.

AWS allows users to only access AWS resources (establish session) using temporary credentials when the users are Federating Access via Active Directory or leveraging a cross account role. 
AWS Administrators can also revoke temporary credentials at any time.

== {section}{counter:paragraph}. Monitor and control remote access sessions. 

include::../../partials/tables.adoc[tag=implemented]
Remote access sessions to the DIS system are all routed through the AWS Networking account and require appropriate authentication to connect to the DIS application environment.
All AWS Application Control Interface (API) commands are logged to CloudTrail.

== {section}{counter:paragraph}. Employ cryptographic mechanisms to protect the confidentiality of remote access sessions. 

include::../../partials/tables.adoc[tag=implemented]
Remote access from DHS laptops is encrypted via Virtual Private Network (VPN) connection to the DHS internal network. 
Remote access to the AWS management console and Application Control Interface (API) utilizes Transport Layer Security (TLS) / Secure Socket Layer (SSL) encryption to secure the remote session.

== {section}{counter:paragraph}. Route remote access via managed access control points. 

include::../../partials/tables.adoc[tag=implemented]
Remote access to DIS system from DHS laptops requires establishing VPN connection to the DHS network using AAL-2 multi-factor authentication.
All access to resources inside AWS Managed Services (AMS) managed accounts, for both DIS and AMS operators, is gated by the use of bastion hosts.
DIS staff can access account instances by logging into a Customer Bastion hosts in the Shared Services account with Active Directory credentials.
Remote access for AMS operators is controlled via DMZ Bastion hosts in the Networking account.

== {section}{counter:paragraph}. Authorize remote execution of privileged commands and remote access to security-relevant information. 

include::../../partials/tables.adoc[tag=implemented]
Remote execution of privileged commands for AWS infrastructure or services requires authentication and appropriate permissions level authorization to the AWS defined Application Control Interface (API).

== {section}{counter:paragraph}. Authorize wireless access prior to allowing such connections.

include::../../partials/tables.adoc[tag=na]
Wireless networks are not part the of DIS system and out-of-scope for the SSP.

== {section}{counter:paragraph}. Protect wireless access using authentication and encryption. 

include::../../partials/tables.adoc[tag=na]
Wireless networks are not part the of DIS system and out-of-scope for the SSP.

== {section}{counter:paragraph}. Control connection of mobile devices. 

include::../../partials/tables.adoc[tag=na]
Mobile devices are not part of the DIS system and out-of-scope for this SSP.

== {section}{counter:paragraph}. Encrypt CUI on mobile devices and mobile computing platforms. 

include::../../partials/tables.adoc[tag=na]
Mobile devices are not part of the DIS system and out-of-scope for this SSP.

== {section}{counter:paragraph}. Verify and control/limit connections to and use of external systems. 

include::../../partials/tables.adoc[tag=implemented]
Connections to external systems, including other State of Georgia systems, is controlled and limited to necessary network ports and protocols via firewall rules as documented in the _DHS DIS Infrastructure Design_ document.
Connections from AWS Managed Services to the DIS system are limited to access that is needed to provide management and support services.

== {section}{counter:paragraph}. Limit use of organizational portable storage devices on external systems. 

include::../../partials/tables.adoc[tag=implemented]
Portable storage is not used to store or process CUI data for the DIS application.
DHS staff laptops and DIS KIOSK hardware is locked down to prevent connection of portable storage devices, such as USB disks.
Portable storage devices can not used in the AWS cloud environment.

== {section}{counter:paragraph}. Control CUI posted or processed on publicly accessible systems.

include::../../partials/tables.adoc[tag=implemented]
The DIS application is not publicly accessible from the Internet or external networks.

Publicly accessible DIS KIOSK hardware located in DHS field offices is locked down to permit only authorized transactions as outlined in the _DIS Use Cases_ document.
Use of the KIOSK hardware for scanning CUI requires a Georgia Gateway user account.
CUI data scanned into the KIOSK hardware is securely transmitted to the DIS application system. 
CUI data is not posted or processed on other publicly accessible hardware.
