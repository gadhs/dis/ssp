= 3.13. System and Communications Protection
:imagesdir: ../../images
:!paragraph:
:doctitle: System and Communications Protection
:docfilesuffix: .adoc
:section: 3.13.

////
Instructions

For each control replace the default text line (include::../../partials/default_control.adoc[]) with the appropriate language either describing the control implementation, or describing why the control does not apply.

In order to import the correct table, set the tag value to none, implemented, planned, or na based on which box you want to be checked.
////

== {section}{counter:paragraph}. Monitor, control, and protect communications (i.e., information transmitted or received by organizational systems) at the external boundaries and key internal boundaries of organizational systems.

include::../../partials/tables.adoc[tag=implemented]

The DIS environment controls and protects organizational communications at the external boundaries and key internal boundaries of organizational systems through the use of security groups in AWS.
Web Application Firewall (AWS WAF) is integrated with the Application Load Balancer to protect DIS application from common application layer exploits.
A security group controls the traffic that is allowed to reach and leave the resources that it is associated with.
For DIS application, security group rules are created for every interface that communicates with DIS to allow traffic based on IP addresses and ports of the interfaces.
By default all traffic is denied to the interface applications.
Similarly, DIS application is also protected internally between the web, app and DB layer using security groups to allow only traffic from the IP addresses and ports that are applicable.
All traffic traversing the DIS Application VPC (Virtual Private Cloud) is captured in VPC flow logs and is monitored and analyzed by AWS Guard Duty service for threat detection.

== {section}{counter:paragraph}. Employ architectural designs, software development techniques, and systems engineering principles that promote effective information security within organizational systems.

include::../../partials/tables.adoc[tag=implemented]

The DIS infrastructure uses defense in depth across various layers.
Security Groups are used to control traffic in and out of the instances.
Web Application Firewall (AWS WAF) is integrated with the Application Load Balancers to protect DIS application from common application layer exploits.
The web, application and database instances are placed in different private subnets and access between the subnets is controlled via security groups. 

IAM (Identity and Access Managements) roles are provisioned for users to access DIS infrastructure and designed taking into consideration least privilege access.

AWS Guard Duty service is enabled for threat detection and it generates alerts for any suspicious activities.

The DIS application has user access controls, role based access and AD security groups in place to promote effective information security within the organizational systems. 

Host based malware protection is installed on all the DIS host servers for performing malware scans on all the  documents uploaded by end users. Any suspicious files detected by malware protection software is immediately quarantined and an alert is sent to the responsible group.

The DIS team is trained for AWS Security best practices and the infrastructure and application design documents are centrally managed in Microsoft Teams.

== {section}{counter:paragraph}. Separate user functionality from system management functionality.

include::../../partials/tables.adoc[tag=implemented]

DIS maintains separation of user functionality from system management functionalities.
AWS infrastructure management for DIS is performed in collaboration by the AMS (AWS Managed Services) team and the DHS Cloud Management team.
The teams log into the DIS application servers using Bastion hosts deployed in the Shared Services AWS account.
There are separate Bastion hosts for the infrastructure (AMS) team which is used to login to the application servers for maintenance activities and another Bastion host for the application team to connect to the application servers for development activities.

The application administrative tasks are performed by the DIS application admin users.
DIS application has web administrative interfaces to perform and maintain admin related configurations.
These interfaces are isolated from the user application interfaces and access controlled.

== {section}{counter:paragraph}. Prevent unauthorized and unintended information transfer via shared system resources.

include::../../partials/tables.adoc[tag=implemented]

Cached resources in the shared file server of the DIS application are accessible based on the user permissions.
Any data from the previous sessions are not accessible to the user in the current session.

For Kiosks, the access is managed at user level and users can view documents based on the access permissions that are defined.
As soon as the user uploads the documents, it gets deleted automatically from the Kiosk hardware. 
DHS makes the KIOSK disk drives in-operational as a precaution during the decommissioning process and then the disks are sent to the state agency responsible for hardware decommissioning and the state agency shreds the hard drives.

== {section}{counter:paragraph}. Implement subnetworks for publicly accessible system components that are physically or logically separated from internal networks.

include::../../partials/tables.adoc[tag=na]

DIS application does not have any publicly accessible system components.
The DIS application Web, App and Database layers are segregated in their own private subnets. 

== {section}{counter:paragraph}. Deny network communications traffic by default and allow network communications traffic by exception (i.e., deny all, permit by exception).

include::../../partials/tables.adoc[tag=implemented]

The inbound and outbound traffic for DIS application instances are controlled via AWS security groups.
Security group rules are implicit, hence all traffic is denied unless an inbound or outbound rule explicitly allows it.
For DIS application, specific security groups are defined to allow traffic to external interfaces as well as to and from the internal application layers such as web app and DB layer.

== {section}{counter:paragraph}. Prevent remote devices from simultaneously establishing non-remote connections with organizational systems and communicating via some other connection to resources in external networks (i.e., split tunneling).

include::../../partials/tables.adoc[tag=implemented]

DHS does not allow remote devices to simultaneously establish non-remote connections with organizational systems and communicating via some other connection to resources in external networks. The system does not allow split tunneling.

== {section}{counter:paragraph}. Implement cryptographic mechanisms to prevent unauthorized disclosure of CUI during transmission unless otherwise protected by alternative physical safeguards.

include::../../partials/tables.adoc[tag=implemented]

End users connecting to the DIS application are required to be connected via the MFA VPN. 
Bastion hosts are provisioned in the AWS Shared Services account and act as entry points to access hosts in the DIS application. 
The DIS Application Load Balancer is configured to use the DIS-owned certificate managed via the AWS Certificate Manager.
The ALB (Application Load Balancer) is configured to listen for https traffic and redirect any http traffic to https.
Hence the access to the application is TLS 1.2 encrypted.
The traffic flowing from the ALB to the target groups (web servers) is also encrypted and only allows https protocol.

== {section}{counter:paragraph}. Terminate network connections associated with communications sessions at the end of the sessions or after a defined period of inactivity.

include::../../partials/tables.adoc[tag=implemented]

Network connections via the VPN are automatically terminated after 24 hours and the application times out after 10 minutes of inactivity.

== {section}{counter:paragraph}. Establish and manage cryptographic keys for cryptography employed in organizational systems.

include::../../partials/tables.adoc[tag=implemented]

DIS establishes and manages cryptographic keys via the AWS Key Management Service (AWS KMS uses FIPS 140-2 validated hardware security modules (HSM)).
DIS uses an AWS managed customer key for data encryption.

== {section}{counter:paragraph}. Employ FIPS-validated cryptography when used to protect the confidentiality of CUI.

include::../../partials/tables.adoc[tag=implemented]

DIS utilizes the AWS Key Management Service (KMS) to provision cryptographic keys for encryption of data.
The AWS Key Management Service uses hardware security modules (HSMs) that have been validated under FIPS 140-2 to protect the confidentiality and integrity of the keys.

== {section}{counter:paragraph}. Prohibit remote activation of collaborative computing devices and provide indication of devices in use to users present at the device.

include::../../partials/tables.adoc[tag=implemented]

DIS application does not permit remote activation of collaborative computing devices. The access to DIS application is restricted via VPN and user access permissions.

== {section}{counter:paragraph}. Control and monitor the use of mobile code.

include::../../partials/tables.adoc[tag=na]

DIS application does not implement mobile code.

== {section}{counter:paragraph}. Control and monitor the use of Voice over Internet Protocol (VoIP) technologies.

include::../../partials/tables.adoc[tag=na]

VOIP technologies are not used or authorized with the DHS DIS application.

== {section}{counter:paragraph}. Protect the authenticity of communications sessions.

include::../../partials/tables.adoc[tag=implemented]

All remote DHS staff connect to the DIS application via the secure VPN connections. 
The DIS Application Load Balancer is configured to use the DIS-owned certificate that is managed via the AWS Certificate Manager.
The ALB (Application Load Balancer) is configured to listen to https traffic and redirect any http traffic to https.
The access to the application is TLS1.2 encrypted to prevent replay attacks.
AWS WAF (Web Application Firewall) is integrated with the DIS Application load balancer and will protect against common web exploits such as SQL injections.

== {section}{counter:paragraph}. Protect the confidentiality of CUI at rest.

include::../../partials/tables.adoc[tag=implemented]

All DIS data is encrypted at rest using the FIPS 140-2 validated cryptographic keys generated via AWS KMS (Key Management Service).
All the block storage volumes (AWS Elastic Block Storage) and shared file storage (AWS FSx ONTAP) devices are encrypted using the AWS KMS customer keys. 

