= 3.3. Audit and Accountability
:imagesdir: ../../images
:!paragraph:
:doctitle: Audit and Accountability
:docfilesuffix: .adoc
:section: 3.3.

////
Instructions

For each control replace the default text line (include::../../partials/default_control.adoc[]) with the appropriate language either describing the control implementation, or describing why the control does not apply.

In order to import the correct table, set the tag value to none, implemented, planned, or na based on which box you want to be checked.
////

== {section}{counter:paragraph}. Create and retain system audit logs and records to the extent needed to enable the monitoring, analysis, investigation, and reporting of unlawful or unauthorized system activity.

include::../../partials/tables.adoc[tag=implemented]
AWS CloudTrail service monitors and records all Application Control Interface (API) account activity across the AWS infrastructure. 
The AWS Production DIS account has CloudTrail logs enabled and the trails are logged in a central S3 bucket in the Log Archive account. 
The Virtual Private Cloud (VPC) Flow Logs are enabled to capture all the traffic traversing the VPCs.

AWS GuardDuty is a threat detection service that is enabled in the AWS DIS account.
GuardDuty continuously monitors the AWS account for:
* Reconnaissance (unusual API activities, unusual failed login patterns, etc...) 
* Instance Compromise (unusual instance activity, unusual network communications, etc...)
* Account compromise (unusual infrastructure launches or attempts to disable CloudTrail logging, etc...)
* Bucket Compromise (suspicious data access patterns, unusual API calls to retrieve data, etc...)

DIS Users and Admins logon to DIS application servers (EC2 instances) via Bastion Hosts set up in the Shared Services account. 
All Bastion audit information is logged in CloudWatch. 
CloudWatch Logs are stored in /{instance id}/var/log/secure.log for Linux Bastions and in /{instance id}/SecurityEventLog for Windows Bastions.

The DIS application audit trails are captured in the audit table in the DIS database, which captures every action performed along with the user details and event time.
The DIS database is backed up with a daily backup schedule and weekly full backup to Amazon S3.

Additional DIS application monitoring logs are captured as WEC and WCC product logs, OHS logs, batch process custom logs and KIOSK logs.

== {section}{counter:paragraph}. Ensure that the actions of individual system users can be uniquely traced to those users so they can be held accountable for their actions.

include::../../partials/tables.adoc[tag=implemented]
Every AWS API action of an individual user is recorded as an event record in the S3 bucket where the CloudTrail audit logs are stored. 
The log event record consists of userName which captures the IAM (Identity and Access Management) user that performed the task. 
If the user has logged in using a federated access, the userIdentity/principalID provides the federated username of the individual system user.
The log record also consists of eventName - indicating what action was performed by the individual user and eventTime - indicating the time at which the individual user performed the event.

The actions of DIS application level individual system users can be traced from the Audit table in the DIS database. 
All user activity is logged as a new record in the audit table with details such as user name, activity performed and action time.

== {section}{counter:paragraph}. Review and update logged events.

include::../../partials/tables.adoc[tag=planned]
The infrastructure team and application team can review and update the audited events as necessary to ensure that the logged events are as per the security guidelines, however, no formal cadence for logged event reviews is currently in place.

Log review requirements and responsibilities for DIS would be defined and formally documented by the application team.
DIS Application logging is not currently reviewed or updated as it is a COTS (Commercial Off The Shelf) product and all logging data is configured by the vendor. For the DIS application, the default logging level is set for normal operations and the application team has the ability to activate debug logging level for more detailed log event.

== {section}{counter:paragraph}. Alert in the event of an audit logging process failure.

include::../../partials/tables.adoc[tag=planned]
For DIS applications logging on EC2 instances, CloudWatch alerts are configured to alert on conditions such as low filesystem space, which could cause a failure in the logging process.
DHS also plans to implement Splunk for log consolidation and security event monitoring in Q1 2023 and Splunk can be configured to alert on logging event failures.

AWS Config continuously tracks the configuration changes that occur among DIS resources and checks whether these changes violate any of the conditions defined in the AWS Config Rules. If a resource violates a rule, AWS Config flags the resource and the rule as non-compliant and sends a notification to AWS Managed Services (AMS) Operations.

== {section}{counter:paragraph}. Correlate audit record review, analysis, and reporting processes for investigation and response to indications of unlawful, unauthorized, suspicious, or unusual activity.

include::../../partials/tables.adoc[tag=planned]
Splunk (to be implemented in Q1 2023) would be used to correlate audit record review of the DIS application for investigation of unlawful, unauthorized, suspicious or unusual activity.

AWS GuardDuty analyzes continuous streams of meta-data generated from the DIS account and network activity found in AWS CloudTrail Events, Amazon VPC Flow Logs, and DNS Logs. 
GuardDuty also uses integrated threat intelligence such as known malicious IP addresses, anomaly detection, and machine learning to identify threats more accurately. 
DIS system environment (Production and UAT accounts) has GuardDuty enabled.

When AMS detects any GuardDuty violation, or imminent threat of violation, of AWS or your security policies, AMS will gather information, including impacted resources and any configuration-related changes. 
AMS provides 24/7/365 follow-the-sun support with dedicated operators actively reviewing and investigating monitoring dashboards, incident queue, and service requests across all managed accounts. 
AMS investigates the findings and will notify the DIS team through the security escalation contacts listed in the DIS account.

== {section}{counter:paragraph}. Provide audit record reduction and report generation to support on-demand analysis and reporting.

include::../../partials/tables.adoc[tag=planned]
Audit record reduction and report generation for on-demand analysis and reporting will be implemented as part of the Splunk deployment (estimated timeline Q1 2023).

== {section}{counter:paragraph}. Provide a system capability that compares and synchronizes internal system clocks with an authoritative source to generate time stamps for audit records.

include::../../partials/tables.adoc[tag=implemented]
Amazon Linux2 instances uses chrony as the default configuration and it is configured to use the Amazon Time Sync Service. 
This service is available at the 169.254.169.123 IP address for any instance running in the VPC. 
All RHEL DIS servers are configured to use chrony for Amazon Time Sync Service to generate accurate timestamps for audit records.

For AWS infrastructure, the clocks of systems used in log retention, processing, and analysis are synced using an authoritative NTP time source servers.
Amazon maintains internal NTP servers for this purpose. 

== {section}{counter:paragraph}. Protect audit information and audit logging tools from unauthorized access, modification, and deletion.

include::../../partials/tables.adoc[tag=planned]
AWS CloudTrail log file integrity validation feature allows security administrators to determine whether a CloudTrail log file was modified or deleted since it as delivered to the Amazon S3 bucket. 
A CloudWatch Alarm implemented that sends notification to AMS operations when API calls are made to create, update, delete or start/stop logging a trail. 
AWS GuardDuty is enabled in the DIS Prod and UAT accounts, which alerts against attempts to disable CloudTrail logging and other unusual API calls.

DIS application audit logs are planned to be forwarded to Splunk and controls will be put into place to prevent against any unauthorized changes such as audit log modification, deletion or deactivation.


== {section}{counter:paragraph}. Limit management of audit logging functionality to a subset of privileged users.

include::../../partials/tables.adoc[tag=implemented]
DIS authorizes access to AWS CloudTrail logs and their management and review to only authorized individuals with a designated responsibility. 
Only DIS Admin users have access to the application audit log table in the DIS database.

DIS authorizes access to AWS CloudTrail logs and their management and review to only authorized individuals with a designated responsibility.
Only Admin users have access to the application audit log table in the DIS database.

Additionally, access to the centralized CloudTrail log archive in the Log Archive account in limited to a subset of privileged security administrators.
